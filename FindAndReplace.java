package formative1;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class FindAndReplace {
    public static void main(String[] args) throws FileNotFoundException {
        String text = "Lorem ipsum dolor sit amet, Lonsectetur adipiscing elit. "
                + "Maecenas dictum nisl sed mi tempus tristique. Suspendisse eu convallis nunc. "
                + "In hac habitasse platea dictumst. Mauris placerat quis nulla ac rhoncus. "
                + "Mauris blandit nisl sit amet facilisis rhoncus. Suspendisse et posuere neque. "
                + "Quisque sapien lectus, posuere id nulla eget, convallis mollis tellus. "
                + "Duis convallis sapien ut nunc facilisis fermentum. Nulla placerat massa quis mattis rutrum. "
                + "Interdum et malesuada fames ac ante ipsum primis in faucibus. "
                + "Aenean scelerisque luctus eros ac volutpat..";
        // huruf L dirubah menjadi angka 7
        String regex_L = "[L]";
        String result1 = text.replaceAll(regex_L,"7");
        System.out.println(result1);
        
        // huruf i dirubah menjadi angka 1
        String regex_i = "[i]";
        String result2 = result1.replaceAll(regex_i,"1");
        System.out.println(result2);
        
        // huruf p dirubah menjadi angka 8
        String regex_p = "[p]";
        String result3 = result2.replaceAll(regex_p,"8");
        System.out.println(result3);
        
        // huruf o dirubah menjadi angka 9
        String regex_o = "[o]";
        String result4 = result3.replaceAll(regex_o,"9");
        System.out.println(result4);
        
        // huruf s dirubah menjadi angka 5
        String regex_s = "[s]";
        String result5 = result4.replaceAll(regex_s,"5");
        System.out.println(result5);
        
        // mengambil semua angka dan simpan menjadi day821.txt
        Pattern patternNumber = Pattern.compile("\\d+");
        Matcher matcherNumber = patternNumber.matcher(result5);
        PrintStream fileNumber = new PrintStream(new File("C:/day821.txt"));
        fileNumber.println(matcherNumber);
        
        // mengambil semua alfabet dan simpan menjadi day822.txt
        Pattern patternChar = Pattern.compile("\\d+");
        Matcher matcherChar = patternChar.matcher(result5);
        PrintStream fileChar = new PrintStream(new File("C:/day822.txt"));
        fileChar.println(matcherNumber);
    }
}
