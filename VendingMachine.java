import java.util.Scanner;
import java.io.IOException;
public class VendingMachine {    
public static void main(String[] args) throws IOException{
    Scanner input=new Scanner(System.in);
    String ulang;
    boolean lanjut=true;
    // membuat array barang
    String[][] array= {
    {"Japota ","Japota ","  Chitato  "," Chitato ","Chitato","Potabee ","Potabee ","Lay's ","Lay's ","Tricks"},
    {"Cadbury","Cadbury","SilverQueen","Toblerone","Monggo "," KitKat "," KitKat ","KitKat","KitKat","Colata"},
    {" Frozz "," Frozz ","Alpenliebe ","Alpenliebe","Kopiko"," Kopiko "," Kopiko "," MinZ "," Minz ","Minz"},
    {"  Aqua ","  Aqua ","    Aqua   ","  Nestle  ","Nestle","Nestle  ","  Vit   "," Vit  "," Ades  ","Ades"},
    {"Sprite ","Sprite "," CocaCola  ","  Fanta   ","Fanta ","Bintang "," Fayrous","Sprite"," A&W   ","Pepsi"},
    {" Pucuk ","Teh Gelas","IchiOcha ","  FresFea ","FruitTea","Javana"," Pucuk  "," Pucuk"," Nii  ","  Nii  "}
    };
        System.out.println("Daftar menu yang tersedia ");
        System.out.println("---------------------------------------------");
        // menampilkan barang dengan looping
        for(int i=0; i<array.length;i++){
            System.out.print("[ ");
            for(int j=0; j< array[i].length;j++){
            System.out.print(array[i][j]+", ");
            }
            System.out.println("]\n");
        }
        // array harga
        int[] harga={10000,20000,5000,3000,7000,4000};
        // looping agar dapat memilih dari awal
        while(lanjut){
            // tes jika baris dan kolom yang dimasukan tidak tersedia
            // mencegah agar tidak terjadi error
            try{
                System.out.println("---------------------------------------------");
                System.out.println("Masukkan koordinat menu yang ingin anda ambil");
                System.out.print("Koordinat baris : ");
                int baris=input.nextInt()-1;
                System.out.print("Koordinat kolom : ");
                int kolom=input.nextInt()-1;

                System.out.println("Menu yang anda pilih adalah "+array[baris][kolom]);
                System.out.println("Harga "+array[baris][kolom]+" adalah Rp "+harga[baris]+"\n");
                boolean kondisi=true;
                // looping jika uang yang dibayarkan kurang
                while(kondisi){
                    System.out.println("\n---------------------------------------------");
                    System.out.print("Silahkan masukkan uang Anda untuk membayar : Rp ");
                    int bayar=input.nextInt();
                    if(bayar>=harga[baris]){
                        int kembali=bayar-harga[baris];
                        System.out.println("Kembalian Anda adalh Rp "+kembali);
                        System.out.println("Silahkan ambil"+array[baris][kolom]+" yang Anda beli.");
                        kondisi=false;

                    }else{
                        System.out.println("Mohon maaf uang Anda kurang.");
                        System.out.println("Silahkan ambil uang Anda. Pastikan uang Anda cukup");
                        System.out.print("Apakah Anda jadi membeli barang ini (ya/tidak) ? ");
                        String bayarUlang=input.next();
                        if(bayarUlang.equalsIgnoreCase("ya")){
                            kondisi=true;
                        }else{
                            kondisi=false;
                        }              
                    }
                }
            }catch(Exception e){
                System.out.println("Koordinat yang Anda masukkan tidak tersedia");
            }
         
           System.out.println("---------------------------------------------");
           System.out.print("Apakah Anda ingin membeli barang lain(Ya/Tidak) ? ");
           String Lanjut=input.next();
           if(Lanjut.equalsIgnoreCase("ya")){
               lanjut=true;
           }else{
               lanjut=false;
           }
        } 
      System.out.println("Terimakasih");
   }
}
